
esim. locator.py
*************
LOCATOR_MAP_OUTOCAL = { "Save":"//owl-button[4]",
                        "First Analyzer":"//div[@id='items']/cx-analyzer-list-item[1]",
                        "Login Button":"//owl-button[2]",
                        "Input Type Username":"css=input[name=\"username\"]",
                        "Input Type Password":"css=input[name=\"password\"]",
						"Site Search":"//div[@id='items']/cx-analyzer-list-item[INDEX1]/div/div/div",
						"Site Search 2":"//div[@id='items']/cx-analyzer-list-item[INDEX1]/div/div/div[INDEX2]",
               }
               
USER_AND_PASSWORDS = { "Admin User":"admin",
                       "Admin User Password":"diipadaa",
                       "Non Valid User":"admin1",
                       "Non Valid User Password":"diipadaa",
}                        



ELEMENT_MAP_OUTOCAL = dict(LOCATOR_MAP_OUTOCAL.items() + USER_AND_PASSWORDS.items() )
********

*** Variables ***
Library  Selenium2Library
Variables   locator.py
Resource	common_resource.txt

${ELEMENT_LOCATION_MAPPING}     ${ELEMENT_MAP_OUTOCAL}
@{SEARCH KEYS LOCATION}   diipadaa   diipadaa2
*** Keywords ***
	[Arguments]    ${name}  ${index1}=0  ${index2}=0
	[Documentation]    Returns valid locator from Dictionary
	${location} =    Get From Dictionary     ${ELEMENT_LOCATION_MAPPING}   ${name}
	${index1}=  Run Keyword If  ${index1} > 0    Convert To String   ${index1}
	${index2}=  Run Keyword If  ${index2} > 0    Convert To String   ${index2}
	Run Keyword And Return If  ${index1} > 0 and '${index2}' == 'None'   Replace String  ${location}  INDEX1  ${index1}
	Run Keyword And Return If  ${index1} > 0 and ${index2} > 0   Return Location  ${location}  ${index1}  ${index2}  
	[Return]	${location}
	
Return Location
	[Arguments]   ${location}   ${index1}  ${index2}
	${location new}=  Replace String  ${location}  INDEX1  ${index1}
	${location}=  Replace String  ${location new}  INDEX2  ${index2}
	[Return]	${location}
	
	
*** Test Case ***
Click Some Elmenent
	${locator1}=  Get From Dict   Login Button
	Click Element  ${locator1}
	:FOR  ${item}  IN  @{SEARCH KEYS LOCATION}
	\  ${locator}=  Get From Dict  Site Search  ${index}
	\  ${data} =  Get Text  ${locator}
	\  log   ${data}
	:FOR  ${item}  IN  @{SEARCH KEYS LOCATION}
	\  ${locator}=  Get From Dict  Site Search 2  ${index}  1
	\  ${data} =  Get Text  ${locator}
	\  log   ${data}